'use strict';

module.exports = {
    port : process.env.PORT || 8080,
    URLtoScrap: 'https://medium.com/',
    MongoURI: 'mongodb://localhost:27017/Rentomojo_Prod'
};
