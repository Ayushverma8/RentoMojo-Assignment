const newman = require('newman'); // require newman in your project
var path = require('path');

var runner = newman.run({
    collection: require('./RentoMojo.postman_collection.json'),
    reporters: 'cli'
}, function (err) {
    if ( err ) {
        throw err;
    }
    console.log('collection run complete!');
});